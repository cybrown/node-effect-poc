function effect(type) {
    return (...args) => ({ type, args });
}

module.exports.echo = effect('ECHO');
module.exports.serverStart = effect('SERVER_START');
module.exports.serverListen = effect('SERVER_LISTEN');
module.exports.serverStop = effect('SERVER_STOP');
module.exports.serverWaitForNextRequest = effect('SERVER_WAIT_FOR_NEXT_REQUEST');
module.exports.responseSend = effect('RESPONSE_SEND');
module.exports.sleep = effect('SLEEP');
module.exports.random = effect('RANDOM');
module.exports.now = effect('NOW');
module.exports.spawn = effect('SPAWN');
module.exports.fail = effect('FAIL');
module.exports.call = effect('CALL');
