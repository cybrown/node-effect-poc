const {
    echo,
    serverStart,
    serverListen,
    serverStop,
    serverWaitForNextRequest,
    responseSend,
    sleep,
    random,
    spawn,
    now,
    fail,
    call
} = require('./effects');
const { Interpreter } = require('./interpreter');

function* failRequest(message) {
    yield echo('Request will fail with message: ' + message + '\n');
    yield fail(message);
}

function* handleRequest({req, res}, doFail) {
    try {
        yield echo(`[${yield now()}] Incoming request to: ${req.url}\n`);
        if (doFail) {
            yield call(failRequest, 'Failure');
        }
        yield sleep(1000);
        yield responseSend(res, 200, `[${yield now()}] request number: ${yield random()}`);
    } catch (e) {
        yield echo('Request failed:\n');
        yield echo(e.stack);
        yield echo('\n');
        yield responseSend(res, 500, `[${yield now()}] request failed`);
    }
}

function* main () {
    try {
        yield echo('Starting server\n');
        const server = yield serverStart();
        yield serverListen(server, 3001);
        yield echo('Server started\n');
        for (let i = 0; i < 10; i++) {
            yield spawn(handleRequest, yield serverWaitForNextRequest(server), i == 1);
        }
        yield echo('Stoping server\n');
        yield serverStop(server);
        yield echo('Server stopped\n');
    } catch (e) {
        yield echo('An error occured:\n');
        yield echo(e.stack);
    }
}

const interpreter = new Interpreter();

interpreter.run(main, () => console.log('Exit main'));
