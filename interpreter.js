const http = require('http');

class ResourceHolder {

    constructor() {
        this.nextId = 1;
        this.values = {};
    }

    get(id) {
        return this.values[id];
    }

    create(value) {
        const id = this.nextId++;
        this.values[id] = value;
        value.id = id;
        return id;
    }
}

class HttpServerWrapper {

    constructor(resourceHolder) {
        this.values = [];
        this.subscribers = [];
        this.server = http.createServer((req, res) => {
            resourceHolder.get(this.id).push({
                req: {
                    id: resourceHolder.create(req),
                    url: req.url
                },
                res: {
                    id: resourceHolder.create(res),
                },
            });
        });
    }

    push(data) {
        if (this.subscribers.length) {
            const subscriber = this.subscribers.shift();
            subscriber(data);
        } else {
            this.values.push(data);
        }
    }

    waitForNextRequest(cb) {
        if (this.values.length) {
            cb(this.values.shift());
        } else {
            this.subscribers.push(cb);
        }
    }

    close(cb) {
        this.server.close(cb);
    }

    listen(port) {
        this.server.listen(port);
    }

    on(...args) {
        this.server.on(...args);
    }
}

class Interpreter {

    constructor() {
        this.resourceHolder = new ResourceHolder();
    }

    run(generator, onExit) {
        return this.continueGenerator(generator(), onExit);
    }

    continueGenerator(gen, onExit, nextValue, nextErr) {
        let yieldedValue;
        if (nextErr != null) {
            try {
                yieldedValue = gen.throw(nextErr)
            } catch (e) {
                onExit(e);
                return;
            }
        } else {
            yieldedValue = gen.next(nextValue)
        }
        const value = yieldedValue.value;
        if (yieldedValue.done) {
            onExit();
            return;
        }
        switch (value.type) {
            case 'ECHO': {
                process.stdout.write(value.args[0], () => this.continueGenerator(gen, onExit));
                break;
            }
            case 'SERVER_START': {
                const server = this.resourceHolder.create(new HttpServerWrapper(this.resourceHolder));
                this.continueGenerator(gen, onExit, server);
                break;
            }
            case 'SERVER_LISTEN': {
                const [serverId, port] = value.args;
                const server = this.resourceHolder.get(serverId);
                server.listen(port);
                server.on('listening', () => this.continueGenerator(gen, onExit));
                server.on('error', err => this.continueGenerator(gen, onExit, null, err));
                break;
            }
            case 'SERVER_WAIT_FOR_NEXT_REQUEST': {
                const serverId = value.args[0];
                this.resourceHolder.get(serverId).waitForNextRequest(data => this.continueGenerator(gen, onExit, data));
                break;
            }
            case 'SERVER_STOP': {
                const serverId = value.args[0];
                this.resourceHolder.get(serverId).close(() => this.continueGenerator(gen, onExit));
                break;
            }
            case 'RESPONSE_SEND': {
                const [res, status, data] = value.args;
                this.resourceHolder.get(res.id).statusCode = status;
                this.resourceHolder.get(res.id).end(data, () => this.continueGenerator(gen, onExit));
                break;
            }
            case 'SLEEP': {
                const [duration] = value.args;
                setTimeout(() => this.continueGenerator(gen, onExit), duration);
                break;
            }
            case 'RANDOM': {
                this.continueGenerator(gen, onExit, Math.random());
                break;
            }
            case 'NOW': {
                this.continueGenerator(gen, onExit, Date.now());
                break;
            }
            case 'SPAWN': {
                const [nextGenerator, ...args] = value.args;
                this.continueGenerator(nextGenerator(...args), () => {
                    console.log('Exit spanwed');
                });
                this.continueGenerator(gen, onExit);
                break;
            }
            case 'FAIL': {
                const [message] = value.args;
                this.continueGenerator(gen, onExit, null, new Error(message));
                break;
            }
            case 'CALL': {
                const [nextGenerator, ...args] = value.args;
                this.continueGenerator(nextGenerator(...args), (err) => this.continueGenerator(gen, onExit, null, err));
                break;
            }
        }
    }
}

module.exports.Interpreter = Interpreter;
